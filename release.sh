#!/bin/bash

set -x
set -e # Exit on error

PACKAGE=$(basename "$PWD")
mkdir dist
TEMPDIR="$(mktemp -d)"
read VERSION < NEWS
echo "$PACKAGE-$VERSION: $TEMPDIR"
hg archive "$TEMPDIR/$PACKAGE-$VERSION"
DIR="$PWD"
cd "$TEMPDIR"
rm "$PACKAGE-$VERSION/release.sh"
rm "$PACKAGE-$VERSION"/.hg*
tar czf "$DIR/dist/$PACKAGE-$VERSION.tar.gz" "$PACKAGE-$VERSION"
tar cjf "$DIR/dist/$PACKAGE-$VERSION.tar.bz2" "$PACKAGE-$VERSION"
mkdir -p SOURCES SPECS BUILD SRPMS RPMS
ln "$DIR/dist/$PACKAGE-$VERSION.tar.gz" SOURCES/
unset LINGUAS CFLAGS
rpmbuild -ba "$PACKAGE-$VERSION/$PACKAGE.spec"
mv "SRPMS/$PACKAGE-$VERSION-1.src.rpm" "$DIR/dist"
mv "RPMS/"*"/$PACKAGE-"{,debuginfo-}"$VERSION-1."*".rpm" "$DIR/dist"
rm -fr "$TEMPDIR"
