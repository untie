PREFIX = /usr/local

PACKAGE = untie
OBJECTS = cmdline.o untie.o
CFLAGS += -Wall

all: $(PACKAGE)

untie: $(OBJECTS)

untie.o: cmdline.h

cmdline.c cmdline.h: $(PACKAGE).ggo NEWS
	gengetopt -u'COMMAND' --set-version=$(shell head -n 1 NEWS) < $<

.PHONY: clean
clean:
	-rm $(OBJECTS) $(PACKAGE)

install: all
	install -d $(PREFIX)/sbin
	install -m 755 $(PACKAGE) $(PREFIX)/sbin/$(PACKAGE)
	install -d $(PREFIX)/share/man/man8/
	gzip < $(PACKAGE).8 > $(PREFIX)/share/man/man8/$(PACKAGE).8.gz

uninstall:
	rm $(PREFIX)/sbin/$(PACKAGE)
	rm $(PREFIX)/share/man/man8/$(PACKAGE).8.gz
