/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * See the COPYING file for license information.
 *
 * Copyright (c) 2006, 2007 Guillaume Chazarain <guichaz@yahoo.fr>
 */

#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <errno.h>
#include <sched.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <grp.h>
#include <pwd.h>
#include <time.h>

/*
 * Copied from linux-2.6/include/linux/sched.h
 * Compile on old libc, run on new kernel :-)
 */
#define CLONE_NEWNS	0x00020000	/* New namespace */
#define CLONE_NEWUTS	0x04000000	/* New utsname */
#define CLONE_NEWIPC	0x08000000	/* New ipcs */
#define CLONE_NEWUSER	0x10000000	/* New user namespace */
#define CLONE_NEWPID	0x20000000	/* New pid namespace */
#define CLONE_NEWNET	0x40000000	/* New network namespace */

#include "cmdline.h"

/*
 * We find out the UID/GID infos before the chroot as we may need /etc/passwd.
 * The results are stored in this struct.
 */
struct user_info {
	uid_t uid;
	gid_t gid;
	size_t nr_groups;
	gid_t *groups;
};

/*
 * The temporary stack for the cloned process
 */
#define STACK_SIZE (1 << 20)
static char stack[STACK_SIZE];

/*
 * Get the clone(2) flags from the command line arguments
 */
static int get_flags(struct gengetopt_args_info *args_info)
{
	int flags = 0;
	unsigned int i;

	if (args_info->mount_flag)
		flags |= CLONE_NEWNS;

	if (args_info->uname_flag)
		flags |= CLONE_NEWUTS;

	if (args_info->ipc_flag)
		flags |= CLONE_NEWIPC;

	if (args_info->userns_flag)
		flags |= CLONE_NEWUSER;

	if (args_info->pidns_flag)
		flags |= CLONE_NEWPID;

	if (args_info->netns_flag)
		flags |= CLONE_NEWNET;

	for (i = 0; i < args_info->mask_given; i++)
		flags |= args_info->mask_arg[i];

	return flags;
}

/*
 * Wrapper for malloc, exit instead of returning NULL
 */
static void *xmalloc(int size)
{
	void *res = malloc(size);
	if (!res) {
		perror("malloc");
		exit(EXIT_FAILURE);
	}
	return res;
}

/*
 * Do a chroot() if requested. Do also a chdir() to prevent the simplest
 * form a chroot escape.
 */
static void perform_chroot(struct gengetopt_args_info *ggo)
{
	if (ggo->chroot_given) {
		if (chroot(ggo->chroot_arg) < 0) {
			perror("chroot");
			exit(EXIT_FAILURE);
		}

		if (chdir("/") < 0) {
			perror("chdir(\"/\")");
			exit(EXIT_FAILURE);
		}
	}
}

/*
 * Find the UID associated to a username
 */
static uid_t name_to_uid(const char *name)
{
	struct passwd *pass;

	errno = 0;
	pass = getpwnam(name);

	if (!pass) {
		if (errno)
			perror("getpwnam");
		else
			fprintf(stderr, "%s: user not found\n", name);
		exit(EXIT_FAILURE);
	}

	return pass->pw_uid;
}

/*
 * Find the GID associated to a group
 */
static gid_t name_to_gid(const char *name)
{
	struct group *grp;

	errno = 0;
	grp = getgrnam(name);

	if (!grp) {
		if (errno)
			perror("getgrnam");
		else
			fprintf(stderr, "%s: group not found\n", name);
		exit(EXIT_FAILURE);
	}

	return grp->gr_gid;
}

/*
 * Transform the command line args (ggo) into our struct (user)
 */
static void get_user_infos(struct user_info *user,
			   struct gengetopt_args_info *ggo)
{
	size_t nr_groups = ggo->gid_given + ggo->groupname_given;

	if (ggo->uid_given && ggo->username_given) {
		fputs("--uid and --username are mutually exclusive\n", stderr);
		exit(EXIT_FAILURE);
	}

	if (ggo->uid_given)
		user->uid = ggo->uid_arg;
	else if (ggo->username_given)
		user->uid = name_to_uid(ggo->username_arg);
	else
		user->uid = -1;

	if (nr_groups)
		user->groups = xmalloc(nr_groups * sizeof(gid_t));
	else
		user->groups = NULL;
	user->nr_groups = 0;

	if (ggo->gid_given) {
		memcpy(user->groups, ggo->gid_arg,
		       ggo->gid_given * sizeof(gid_t));
		user->nr_groups = ggo->gid_given;
	}

	if (ggo->groupname_given) {
		int i;
		for (i = 0; i < ggo->groupname_given; i++) {
			gid_t gid = name_to_gid(ggo->groupname_arg[i]);
			user->groups[user->nr_groups] = gid;
			user->nr_groups++;
		}
	}

	if (user->nr_groups)
		user->gid = user->groups[0];
	else {
		user->gid = user->uid;
		if (user->uid != -1) {
			user->nr_groups = 1;
			user->groups = &user->uid;
		}
	}
}

/*
 * Change the UID and GID if requested.
 */
static void change_id(struct user_info *user)
{
	if (user->nr_groups) {
		if (setgroups(user->nr_groups, user->groups) < 0) {
			perror("setgroups");
			exit(EXIT_FAILURE);
		}
	}

	if (user->gid != -1) {
		if (setgid(user->gid) < 0) {
			perror("setgid");
			exit(EXIT_FAILURE);
		}
	}

	if (user->uid != -1) {
		if (setuid(user->uid) < 0) {
			perror("setuid");
			exit(EXIT_FAILURE);
		}
	}
}

/*
 * Run as a daemon
 */
static void daemonize(void)
{
	int fd;

	switch (fork()) {
	case -1:
		perror("fork");
		exit(EXIT_FAILURE);
	case 0:
		break;
	default:
		exit(EXIT_SUCCESS);
	}

	if (setsid() < 0)
		perror("setsid");

	if (chdir("/") < 0)
		perror("chdir(\"/\")");

	fd = open("/dev/null", O_RDWR, 0);
	if (fd >= 0) {
		if (dup2(fd, STDIN_FILENO) < 0)
			perror("dup2(fd, STDIN_FILENO)");
		if (dup2(fd, STDOUT_FILENO) < 0)
			perror("dup2(fd, STDOUT_FILENO)");
		if (dup2(fd, STDERR_FILENO) < 0)
			perror("dup2(fd, STDERR_FILENO)");
		if (fd > STDERR_FILENO)
			close(fd);
	} else
		perror("open(\"/dev/null\")");
}

/*
 * Configure a (posssibly RT) scheduling policy
 */
static void set_scheduler(struct gengetopt_args_info *ggo)
{
	static struct {
		const char *name;
		int value;
	} policies[] = {
		/* *INDENT-OFF* */
		{"fifo",   SCHED_FIFO},
		{"rr",     SCHED_RR},
		{"other",  SCHED_OTHER},
		{"normal", SCHED_OTHER}
		/* *INDENT-ON* */
	};
	struct sched_param param;

	if (ggo->sched_given) {
		int policy;
		int policy_found = 0;
		int i;

		for (i = 0; i < sizeof(policies) / sizeof(policies[0]); i++)
			if (!strcasecmp(ggo->sched_arg, policies[i].name)) {
				policy = policies[i].value;
				policy_found = 1;
				break;
			}

		if (!policy_found) {
			char *end;
			policy = strtol(ggo->sched_arg, &end, 0);
			if (ggo->sched_arg[0] == '\0' || end[0] != '\0') {
				fprintf(stderr,
					"Unknown scheduling policy: %s\n",
					ggo->sched_arg);
				exit(EXIT_FAILURE);
			}
		}

		if (ggo->schedprio_given)
			param.sched_priority = ggo->schedprio_arg;
		else {
			param.sched_priority = sched_get_priority_min(policy);
			if (param.sched_priority < 0) {
				perror("sched_get_priority_min");
				exit(EXIT_FAILURE);
			}
		}

		if (sched_setscheduler(0, policy, &param) < 0) {
			perror("sched_setscheduler");
			exit(EXIT_FAILURE);
		}
	} else if (ggo->schedprio_given) {
		param.sched_priority = ggo->schedprio_arg;
		if (sched_setparam(0, &param) < 0) {
			perror("sched_setparam");
			exit(EXIT_FAILURE);
		}
	}
}

static pid_t child_pid;

static void kill_child()
{
	if (child_pid > 0)
		kill(child_pid, SIGKILL);
}

static void deadly_signal_handler(int sig)
{
	kill_child();
}

/*
 * If the parent is killed, it must kill its child too
 */
static void install_signals_handlers(void)
{
	int signals[] = { SIGHUP, SIGINT, SIGQUIT, SIGTERM };
	int i;

	for (i = 0; i < sizeof(signals) / sizeof(signals[0]); i++)
		signal(signals[i], deadly_signal_handler);
}

/*
 * Exit when a SIGCHLD signals that the child has exited
 */
static void sig_chld_handler(int sig, siginfo_t * info, void *data)
{
	int code;

	kill_child();

	code = info->si_status;
	if (info->si_code & CLD_KILLED) {
		raise(code);
		code += 128;
	}

	exit(code);
}

/*
 * Configure the SIGCHLD handler to detect the child termination and propagate
 * its exit status.
 */
static void install_sigchld_handler(struct gengetopt_args_info *ggo)
{
	struct sigaction act;

	act.sa_sigaction = sig_chld_handler;
	act.sa_flags = SA_SIGINFO | SA_NOCLDSTOP;
	sigemptyset(&act.sa_mask);

	if (sigaction(SIGCHLD, &act, NULL) < 0) {
		perror("sigaction");
		exit(EXIT_FAILURE);
	}
}

/*
 * Check double parameters to be used as struct timespec
 */
static void check_double_as_timespec(double seconds)
{
	time_t as_integer = (time_t) seconds;
	double delta = seconds - as_integer;

	if (seconds <= 0.0 || as_integer < 0) {
		fprintf(stderr, "Invalid (negative?) timeout: %lf\n", seconds);
		exit(EXIT_FAILURE);
	}

	if (delta < 0.0 || delta >= 1.0) {
		fprintf(stderr, "Invalid (overflow?) timeout: %lf\n", seconds);
		exit(EXIT_FAILURE);
	}
}

/*
 * Kill the child after the specified delay
 */
static void wait_to_kill(double seconds, int sig)
{
	struct timespec sleep_time;

	sleep_time.tv_sec = (time_t) seconds;
	sleep_time.tv_nsec = (long)((seconds - sleep_time.tv_sec) * 1e9);

	if (nanosleep(&sleep_time, NULL) < 0) {
		perror("nanosleep");
		exit(EXIT_FAILURE);
	}

	if (kill(child_pid, sig) < 0) {
		perror("kill");
		exit(EXIT_FAILURE);
	}
}

/*
 * Kill the child according to the two possible delays
 */
static void watchdog(struct gengetopt_args_info *ggo)
{
	double delay;
	int sig;

	if (!ggo->timeout_term_given && !ggo->timeout_kill_given)
		return;

	if (ggo->daemonize_flag) {
		fputs("--timeout-XXXX is incompatible with --daemonize\n",
		      stderr);
		exit(EXIT_FAILURE);
	}

	if (!ggo->timeout_term_given || (ggo->timeout_kill_given &&
					 ggo->timeout_kill_arg <
					 ggo->timeout_term_arg)) {
		/* SIGKILL before SIGTERM? Why not ;-) */
		sig = SIGKILL;
		delay = ggo->timeout_kill_arg;
	} else {
		sig = SIGTERM;
		delay = ggo->timeout_term_arg;
	}

	wait_to_kill(delay, sig);

	if (ggo->timeout_term_given != ggo->timeout_kill_given)
		return;

	delay = ggo->timeout_kill_arg - ggo->timeout_term_arg;
	if (delay < 0.0)
		delay = -delay;

	/* The other signal */
	sig = SIGKILL + SIGTERM - sig;
	wait_to_kill(delay, sig);
}

/*
 * The new process in its own namespace can now execute the command passed as
 * argument or a shell
 */
static int child_process(void *ggo_arg)
{
	struct gengetopt_args_info *ggo = ggo_arg;
	struct user_info user;

	set_scheduler(ggo);

	if (ggo->nice_given)
		if (setpriority(PRIO_PROCESS, 0, ggo->nice_arg) < 0) {
			perror("setpriority");
			return EXIT_FAILURE;
		}

	get_user_infos(&user, ggo);
	perform_chroot(ggo);
	change_id(&user);

	if (ggo->daemonize_flag)
		daemonize();

	if (ggo->alarm_given)
		alarm(ggo->alarm_arg);

	if (ggo->inputs_num) {
		char **argv = xmalloc(sizeof(char *) * (ggo->inputs_num + 1));
		memcpy(argv, ggo->inputs, ggo->inputs_num * sizeof(char *));
		argv[ggo->inputs_num] = NULL;
		execvp(argv[0], argv);
		perror("execvp");
	} else {
		char *argv[] = { NULL, NULL };
		char *shell = getenv("SHELL");
		if (!shell)
			shell = "/bin/sh";
		argv[0] = shell;
		execv(argv[0], argv);
		perror("execv");
	}

	return EXIT_FAILURE;
}

int main(int argc, char *argv[])
{
	struct gengetopt_args_info args_info;
	int ret;
	int flags;
	int fork_needed = 0;

	ret = cmdline_parser(argc, argv, &args_info);
	if (ret)
		return ret;
	if (argc - args_info.inputs_num <= 1) {
		fputs("No flag specified, exiting\n", stderr);
		return EXIT_FAILURE;
	}

	if (args_info.alarm_given &&
	    (args_info.alarm_arg < 0 || args_info.alarm_arg > 65535)) {
		fprintf(stderr,
			"The alarm value must be between 0 and 65535 got: %d\n",
			args_info.alarm_arg);
		return EXIT_FAILURE;
	}

	if (args_info.timeout_term_given)
		check_double_as_timespec(args_info.timeout_term_arg);
	if (args_info.timeout_kill_given)
		check_double_as_timespec(args_info.timeout_kill_arg);

	flags = get_flags(&args_info);
	fork_needed = flags != 0 || args_info.timeout_term_given ||
	    args_info.timeout_kill_given;

	if (fork_needed) {
		atexit(kill_child);
		install_sigchld_handler(&args_info);
		install_signals_handlers();
		flags |= SIGCHLD;
		child_pid = clone(child_process, &stack[STACK_SIZE - 1], flags,
				  &args_info);
		if (child_pid < 0) {
			perror("clone");
			return EXIT_FAILURE;
		}

		watchdog(&args_info);
		pause();
	} else
		ret = child_process(&args_info);

	return ret;
}
