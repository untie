Summary: Process namespace creator
Name: untie
Version: 0.4
Release: 1
License: GPL
Group: System/Utilities
Packager: Guillaume Chazarain <guichaz@yahoo.fr>
URL: http://guichaz.free.fr/untie
Source: http://guichaz.free.fr/untie/files/untie-%{version}.tar.gz
BuildRoot: /var/tmp/%{name}-%{version}-root

%description
untie is a tool used to launch commands in new namespaces.


%prep
%setup -q


%build
make


%install
rm -rf
%makeinstall PREFIX=%{buildroot}/%{_prefix}


%clean
rm -rf


%files
%defattr(-,root,root)
%doc COPYING NEWS README
%{_sbindir}/*
%doc %{_mandir}/*/*
